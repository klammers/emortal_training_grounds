﻿using UnityEngine;



namespace Emortal.Cameras
{
    public class EF_FirstPerson_Camera : EF_Base_Camera 
    {
        #region Varaibles
        #endregion

        #region Methods
        protected override void UpdateTranslation()
        {
            base.UpdateTranslation();
        }

        protected override void UpdateRotation()
        {
            base.UpdateRotation();
        }
        #endregion
    }
}
