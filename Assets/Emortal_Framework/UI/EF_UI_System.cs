﻿using System.Collections;
using System.Collections.Generic;
using Emortal.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Emortal.UI
{
    public class EF_UI_System : MonoBehaviour 
    {
        #region Variables
        [Header("UI System Properties")]
        public Image m_FadeOverlay;
        public float m_FadeDuration = 0.25f;
        #endregion

        #region Main Methods
    	// Use this for initialization
    	void Start () 
        {
            if(m_FadeOverlay)
            {
                m_FadeOverlay.gameObject.SetActive(true);
            }
            FadeIn();
    	}
    	
    	// Update is called once per frame
    	void Update () 
        {
    		
    	}
        #endregion


        #region Utility Methods
        public void LoadScene(int sceneIndex)
        {
            EF_LoadScreen_Manager.LoadScene(sceneIndex);
        }

        public void FadeIn()
        {
            if(m_FadeOverlay)
            {
                m_FadeOverlay.CrossFadeAlpha(0, m_FadeDuration, true); 
            }
        }

        public void FadeOut()
        {
            if(m_FadeOverlay)
            {
                m_FadeOverlay.CrossFadeAlpha(1, m_FadeDuration, true);
            }
        }
        #endregion
    }
}
