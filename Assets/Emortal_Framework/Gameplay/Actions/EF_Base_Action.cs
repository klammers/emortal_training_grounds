﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Gameplay
{
    public class EF_Base_Action : MonoBehaviour
    {
        #region Variables
        public Dictionary<string, EF_Base_Action> m_triggerData = new Dictionary<string, EF_Base_Action>();
        //data used by actions/events
        #endregion
        #region Methods
        public virtual Dictionary<string, EF_Base_Action> TriggerData
        {
           get { return m_triggerData; }
        }

        public virtual void Fire()
        {
        }
    }
        #endregion
}

