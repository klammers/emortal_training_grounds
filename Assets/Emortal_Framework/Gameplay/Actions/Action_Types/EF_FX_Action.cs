﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

namespace Emortal.Gameplay
{
    public class EF_FX_Action : EF_Base_Action
        {
        #region Variables
        [Header("FX Action Properties")]
        public GameObject m_FxObject;
        #endregion

        #region Custom Methods
        public void FXEvent()
        {
            Debug.Log("FX event triggered");
            Debug.Log("Triggers: " + m_triggerData.Count);
        }

        public override void Fire()
        {
            FXEvent();
        }
        #endregion
     }
 }