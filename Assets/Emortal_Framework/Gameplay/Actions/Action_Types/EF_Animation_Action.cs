﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Emortal.Gameplay
{
    public class EF_Animation_Action : EF_Base_Action
    {
        #region Variables
        [Header("Animation Action Properties")]
        public GameObject m_animationObject;
        public string m_animationState = "";
        [HideInInspector]
        public Animator m_animator;

        #endregion
        #region Main Methods
        public void Start()
        { 
            m_animator = m_animationObject.GetComponent<Animator>();
            Debug.Log(m_animator);
            if (m_animationObject == null)
            {
                m_animationObject = gameObject;
            }
        }
        #endregion

        #region Custom Methods
        public void PlayAnimation()
        {
            m_animator.Play(m_animationState);
            //Debug.Log(m_triggerData.Count);
        }

        public override void Fire()
        {
            PlayAnimation();
        }
        #endregion
    }
}
