﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Gameplay
{
    public class EF_Debug_Log_Action: EF_Base_Action
    {
        #region Variables
        [SerializeField]
        public string stringToLog;
        #endregion

        #region Methods
        public override void Fire()
        {
            Debug.Log(stringToLog);
        }
        #endregion
    }
}
