﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    
    public class EF_Base_Trigger : MonoBehaviour
    {
        #region Variables
        public bool m_triggerUnityEvent; //flag
        public UnityEvent m_unityEvent;
        
        protected bool m_activated;
        #endregion

        #region Delegate
        public delegate void TriggerActionDelegate();
        public TriggerActionDelegate m_triggerDelegate;
        #endregion

        #region Virtual Methods
        protected virtual void OnTriggerEnter(Collider other) { }
        #endregion

        #region Custom Methods
        protected virtual void SetTriggerData() { }

        protected void Trigger()
        {
            if(m_triggerUnityEvent)
            {
                m_unityEvent.Invoke();
            }
            if (!m_triggerUnityEvent)
            {
                if(m_triggerDelegate!= null)
                {
                    m_triggerDelegate.Invoke();
                }

            }
        }

        public struct TriggerData
        {
            public GameObject m_triggerObject;
            public EF_Base_Action m_triggeredAction;
        }
        #endregion
    }
}
