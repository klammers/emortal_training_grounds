﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    public class EF_Custom_UnityEvent : MonoBehaviour
    {
        #region Variables
        [SerializeField]
        public EF_Base_Trigger customTrigger;

        [Header("Create event for trigger:")]
        public UnityEvent CustomTriggerEvent;
        #endregion

        #region Methods
        public bool GetConditional()
        {
            //TODO enable developers to enter their own conditional in the Editor
            return true;
        }

        void YourFunction()
        {
            if (GetConditional())
            {
                CustomTriggerEvent.Invoke();
            }
        }
        #endregion
    }
}
