using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    public class EF_Keypress_Trigger : EF_Base_Trigger
    {
        #region Variables
        public string m_keyPressed;
        //TODO flag for the custom inspector
        public EF_Base_Action m_action = new EF_Base_Action();
        #endregion

        #region Methods
        protected void Start()
        {
            m_action = GetComponent<EF_Base_Action>();
            if (m_action != null)
            {
                m_triggerDelegate += m_action.Fire;
            }
            if (m_action == null && !m_triggerUnityEvent)
            {
                throw new Exception("No action or event assigned to " + this + " on gameObject: " + gameObject);
            }
        }

        private void Update()
        {
            if (Input.GetKey(m_keyPressed))
            {
                Trigger();
            }
        }
        #endregion
    }
}
