using UnityEngine;
using System.Collections;

namespace Emortal.Gameplay
{
    //A trigger that is activated automatically on start
    public class EF_Start_Trigger : EF_Base_Trigger
    {
        void Start()
        {
            Trigger();
        }
    }
}
