using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    public class EF_ButtonPress_Trigger : EF_Base_Trigger
    {
        #region Variables
        public string m_buttonPressed;
        public EF_Base_Action m_action = new EF_Base_Action();
        #endregion

        #region Methods
        protected void Start()
        {
            
            if (m_action != null)
            {
                m_triggerDelegate += m_action.Fire;
            }
            if (m_action == null && !m_triggerUnityEvent)
            {
                throw new Exception("No action or event assigned to " + this + " on gameObject: " + gameObject);
            }
        }

        private void FixedUpdate()
        {
            
            if (Input.GetButton(m_buttonPressed))
            {
                Trigger();
            }
        }
        #endregion
    }
}
