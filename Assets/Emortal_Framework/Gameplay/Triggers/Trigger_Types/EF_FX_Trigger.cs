﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    public class EF_FX_Trigger : EF_Base_Trigger
    {
        #region Variables
        [Header("FX Trigger Properties")]
        public string m_collisionTag = "";
        public EF_FX_Action fxAction;
        private GameObject collision;
        TriggerData fxData;
        #endregion

        #region Methods
        protected override void SetTriggerData()
        {
            fxData = new TriggerData();
            fxData.m_triggeredAction = fxAction;
            fxData.m_triggerObject = collision;
        }
        
        protected override void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(m_collisionTag))
            {
                collision = other.gameObject;
                SetTriggerData();
                m_triggerDelegate += fxAction.Fire;
                fxAction.m_triggerData.Add(fxData.m_triggerObject.name, fxData.m_triggeredAction);
            }
            Trigger();
        }
        #endregion
    }
}