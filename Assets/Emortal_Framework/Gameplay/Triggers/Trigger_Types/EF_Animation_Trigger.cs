﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emortal.Core;
using System.Reflection;

namespace Emortal.Gameplay
{
    public class EF_Animation_Trigger : EF_Base_Trigger
    {
        #region Variables
       [Header("Animation Trigger Properties")]
        public string m_collisionTag = "";
        public EF_Animation_Action m_animationAction;
        private GameObject m_collision;
        TriggerData animationData;
        #endregion

        protected override void SetTriggerData()
        {
            animationData = new TriggerData();
            animationData.m_triggeredAction = m_animationAction;
            animationData.m_triggerObject = m_collision;
        }


        #region Methods
        protected override void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(m_collisionTag))
            {
                m_collision = other.gameObject;
                SetTriggerData();
                m_triggerDelegate += m_animationAction.Fire;
                m_animationAction.m_triggerData.Add(animationData.m_triggerObject.name, animationData.m_triggeredAction) ;
                //m_animationAction.m_triggerData.Add(animationData.m_triggeredAction.GetType().ToString(), (animationData.m_triggerObject));
            }
            Trigger();
        }
        #endregion
    }
}

