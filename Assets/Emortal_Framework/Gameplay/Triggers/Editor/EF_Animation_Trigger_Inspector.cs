﻿
using System.Collections;
using System.Collections.Generic;
using Emortal.Gameplay;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    [CustomEditor(typeof(EF_Animation_Trigger))]
    [CanEditMultipleObjects]
    public class EF_Animation_Trigger_Inspector : EF_Base_Trigger_Inspector
    {
        #region Variables
        EF_Animation_Trigger m_Target;
        SerializedProperty m_collisionTag;
        SerializedProperty m_events;
        SerializedProperty m_action;
        #endregion

        #region Main Methods
        void OnEnable()
        {
            m_Target = (EF_Animation_Trigger)target;
            m_collisionTag = serializedObject.FindProperty("m_collisionTag");
            m_events = serializedObject.FindProperty("m_unityEvents");
            m_action = serializedObject.FindProperty("m_animationAction");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }

        protected override void DrawHeader(string aTitle)
        {
            base.DrawHeader("Animation Trigger");

        }

        protected override void DrawBody()
        {
            //base.DrawBody();
            m_Target.m_triggerUnityEvent = EditorGUILayout.Toggle("Trigger Unity Event", m_Target.m_triggerUnityEvent);
            EditorGUILayout.Space();
            if (!m_Target.m_triggerUnityEvent)
            {
                EditorGUILayout.Separator();
                EditorGUILayout.LabelField("Animation Properties", EditorStyles.boldLabel);
                Rect rect = new Rect(0, 0, Screen.width, Screen.height);
                EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandWidth(true));
                EditorGUILayout.Space();
                EditorGUILayout.HelpBox("The tag used to identify objects that trigger animation during collision", MessageType.None, true);
                m_Target.m_collisionTag = EditorGUILayout.TextField("Collision tag: ", m_Target.m_collisionTag);
                EditorGUILayout.HelpBox("Reference to the object containing the action script", MessageType.None, true);
                EditorGUILayout.PropertyField(m_action, new GUIContent("Animation object"));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
            else
            {
                base.DrawBody();
            }
        }
        #endregion
    }
}

