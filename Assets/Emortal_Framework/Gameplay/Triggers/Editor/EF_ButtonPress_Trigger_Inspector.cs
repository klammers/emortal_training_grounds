﻿using System.Collections;
using System.Collections.Generic;
using Emortal.Gameplay;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Emortal.Gameplay
{
    [CustomEditor(typeof(EF_ButtonPress_Trigger))]
    [CanEditMultipleObjects]
    public class EF_ButtonPress_Trigger_Inspector : EF_Base_Trigger_Inspector
    {
        #region Variables
        EF_ButtonPress_Trigger m_Target;
        SerializedProperty m_action;
        SerializedProperty m_events;
        #endregion

        #region Main Methods

        void OnEnable()
        {
            m_Target = (EF_ButtonPress_Trigger)target;
            m_action = serializedObject.FindProperty("m_action");
            m_events = serializedObject.FindProperty("m_unityEvents");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }

        protected override void DrawHeader(string aTitle)
        {
            base.DrawHeader("Keypress Trigger");
            EditorGUILayout.HelpBox("To use a key press as a UnityEvent trigger, check the box below.", MessageType.None, true);

        }

        protected override void DrawBody()
        {
          

            m_Target.m_triggerUnityEvent = EditorGUILayout.Toggle("Trigger Unity Event", m_Target.m_triggerUnityEvent);
            if (!m_Target.m_triggerUnityEvent)
            {
                Rect rect = new Rect(0, 0, Screen.width, Screen.height);
                EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandWidth(true));
                EditorGUILayout.Space();
                m_Target.m_buttonPressed = EditorGUILayout.TextField("Button trigger: ", m_Target.m_buttonPressed);
                EditorGUILayout.PropertyField(m_action, new GUIContent("Action object"));
                EditorGUILayout.Space();
                EditorGUILayout.EndVertical();
            }
            else
            {
                base.DrawBody();
            }



        }
        #endregion
    }
}

