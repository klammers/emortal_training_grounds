﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Gameplay
{
    public class EF_Base_Weapon : MonoBehaviour 
    {

        #region Variables
        public GameObject m_Projectile;
        #endregion

        #region Main Methods
        #endregion

        #region Custom Methods
        public void FireWeapon()
        {
            HandleWeaponFire();
        }

        protected virtual void HandleWeaponFire(){}
        #endregion
    }
}
