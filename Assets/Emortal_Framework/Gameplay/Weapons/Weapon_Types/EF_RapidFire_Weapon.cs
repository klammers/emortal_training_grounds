﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Gameplay
{
    public class EF_RapidFire_Weapon : EF_Base_Weapon 
    {
        #region Variables
        public float m_FireRate = 0.1f;
        public Transform m_Muzzle;

        private float m_LastFireTime = 0f;
        #endregion

        #region Main Methods
        void Start()
        {
            m_LastFireTime = Time.time;
        }
        #endregion

        #region Custom Methods
        protected override void HandleWeaponFire()
        {
            if(Time.time > m_LastFireTime)
            {
                if(m_Muzzle && m_Projectile)
                {
                    //Debug.Log("Firing Rapid Fire Weapon!");
                    Instantiate(m_Projectile, m_Muzzle.position, Quaternion.LookRotation(m_Muzzle.forward));
                }
                m_LastFireTime = Time.time + m_FireRate;
            }
        }
        #endregion
    }
}
