﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Core
{
    public class EF_Dictionary<TK, TV> : ISerializationCallbackReceiver
    {
        private Dictionary<TK, TV> m_Dictionary = new Dictionary<TK, TV>();
        [SerializeField]
        List<TK> m_Keys;
        [SerializeField]
        List<TV> m_Values;

        public void OnBeforeSerialize()
        {
            m_Keys.Clear();
            m_Values.Clear();

            foreach (var kvp in m_Dictionary)
            {
                m_Keys.Add(kvp.Key);
                m_Values.Add(kvp.Value);
            }
        }


        public void OnAfterDeserialize()
        {
            m_Dictionary = new Dictionary<TK, TV>();

            for (int i = 0; i != Mathf.Min(m_Keys.Count, m_Values.Count); i++)
            {
                m_Dictionary.Add(m_Keys[i], m_Values[i]);
            }
        }

        public void Add(KeyValuePair<TK, TV> item)
        {
            Add(item.Key, item.Value);
        }

        public void Add(TK key, TV value)
        {
            m_Keys.Add(key);
            m_Values.Add(value);
        }

        public Dictionary<TK, TV> GetDictionary
        {
            get { return m_Dictionary; }
        }

        public void GetKVP()
        {
            foreach (var kvp in m_Dictionary)
            {
                GUILayout.Label("Key: " + kvp.Key + " Value: " + kvp.Value);
            }
        }
    }
}