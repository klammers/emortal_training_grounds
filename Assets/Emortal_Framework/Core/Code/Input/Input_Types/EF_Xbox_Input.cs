﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Core
{
    public class EF_Xbox_Input : EF_Base_Inputs 
    {
        //Analog Sticks
        public Vector2 LeftStick
        {
            get{return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));}
        }

        public Vector2 RightStick
        {
            get{return new Vector2(Input.GetAxis("XRightHorizontal"), Input.GetAxis("XRightVertical"));}
        }


        //Buttons
        public float AButton
        {
            get{return Input.GetAxis("Fire1");}
        }

        public float BButton
        {
            get{return Input.GetAxis("XBButton");}
        }

        public float XButton
        {
            get{return Input.GetAxis("XXButton");}
        }

        public float YButton
        {
            get{return Input.GetAxis("XYButton");}
        }


        //Triggers
        public float Triggers
        {
            get{return Input.GetAxis("XTriggers");}
        }

    }
}
