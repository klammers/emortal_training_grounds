﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Core
{
    public class EF_Mouse_Input : EF_Base_Inputs 
    {
        public bool LeftClickHold
        {
            get{return Input.GetMouseButton(0);}
        }

        public bool MiddleClickHold
        {
            get{return Input.GetMouseButton(2);}
        }

        public bool RightClickHold
        {
            get{return Input.GetMouseButton(1);}
        }

        public bool LeftClickDown
        {
            get{return Input.GetMouseButtonDown(0);}
        }

        public bool MiddleClickDown
        {
            get{return Input.GetMouseButtonDown(2);}
        }

        public bool RightClickDown
        {
            get{return Input.GetMouseButtonDown(1);}
        }

        public bool LeftClickUp
        {
            get{return Input.GetMouseButtonUp(0);}
        }

        public bool MiddleClickUp
        {
            get{return Input.GetMouseButtonUp(2);}
        }

        public bool RightClickUp
        {
            get{return Input.GetMouseButtonUp(1);}
        }

        public Vector2 MouseDelta
        {
            get{return new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));}
        }

        public float MouseScrollWheel
        {
            get{return Input.GetAxis("Mouse ScrollWheel");}
        }
    }
}
