﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emortal.Core
{
    public class EF_Keyboard_Input : EF_Base_Inputs 
    {
        #region Variables
        public float Vertical
        {
            get{return Input.GetAxis("Vertical");}
        }

        public float Horizontal
        {
            get{return Input.GetAxis("Horizontal");}
        }

        public float LeftArrow
        {
            get{return Input.GetKey(KeyCode.LeftArrow) ? -1f : 0;}
        }

        public float RightArrow
        {
            get{return Input.GetKey(KeyCode.RightArrow) ? 1f : 0;}
        }

        public float UpArrow
        {
            get{return Input.GetKey(KeyCode.UpArrow) ? 1f : 0;}
        }

        public float DownArrow
        {
            get{return Input.GetKey(KeyCode.DownArrow) ? -1f : 0;}
        }

        public bool SpaceBtn
        {
            get{return Input.GetKey(KeyCode.Space);}
        }
        #endregion
    }
}
