﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Emortal.Core
{
    public static class EF_Editor_Utils 
    {
        public static void DisplayDialogBox(string aMessage)
        {
            EditorUtility.DisplayDialog("Emortal Warning", aMessage, "OK");
        }
    }
}
