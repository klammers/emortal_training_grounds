﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Emortal.Core
{
    public class EF_Rename_Window : EF_Base_Window

    {
        #region Variables
        static EF_Rename_Window m_Win;
        private string m_prefix = "";
        string m_suffix = "";
        string m_searchString = "";
        string m_replaceString = "";
        string m_searchRoot = "";
      
        string m_dialogName = "Rename Object";
        #endregion



        #region Main Methods
        public static void InitWindow()
        {
            m_Win = GetWindow<EF_Rename_Window>(true, "Rename",true);
            m_Win.Show();
        }

        void OnEnable()
        {
            Selection.selectionChanged += GetSelected;
        }

        void OnDestroy()
        {
            Selection.selectionChanged -= GetSelected;
            m_suffix = m_prefix = "";
        }




        void OnGUI()
        {
            GUI.backgroundColor = Color.white;
            GUI.contentColor = Color.cyan;
            EditorGUILayout.LabelField("Objects Selected: " + m_SelectedObjects.Length);
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            m_prefix = EditorGUILayout.TextField("Prefix: ", m_prefix, GUILayout.ExpandHeight(true));
            if (GUILayout.Button("Add Prefix", GUILayout.ExpandHeight(true)))
            {
                if (m_SelectedObjects.Length == 1)
                {
                    m_SelectedObjects[0].name = string.Concat(m_prefix, m_SelectedObjects[0].name);
                }
                else
                {
                    AddPrefix();
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            m_suffix = EditorGUILayout.TextField("Suffix: ", m_suffix, GUILayout.ExpandHeight(true));
            if (GUILayout.Button("Add Suffix", GUILayout.ExpandHeight(true)))
            {
                if (m_SelectedObjects.Length == 1)
                {
                    m_SelectedObjects[0].name = string.Concat(m_SelectedObjects[0].name, m_suffix);
                }
                else
                {
                    AddSuffix();
                }
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
            m_searchString = EditorGUILayout.TextField("Find: ", m_searchString, GUILayout.ExpandHeight(true));
            m_replaceString = EditorGUILayout.TextField("Replace: ", m_replaceString, GUILayout.ExpandHeight(true));
            if (GUILayout.Button("Search/Replace", GUILayout.ExpandHeight(true)))
            {
                Replace();
            }
            if (GUILayout.Button("Number Objects", GUILayout.ExpandHeight(true)))
            {
                NumberObjects();
            }
            if (m_Win)
            {
                m_Win.Repaint();
            }
         
        }
        #endregion
        #region Custom Methods
        void AddPrefix()
        { 
            foreach (GameObject obj in Selection.objects)
            {
                obj.name = string.Concat(m_prefix, obj.name);
            }
        }

        void AddSuffix()
        {
            foreach (GameObject obj in m_SelectedObjects)
            {
                obj.name = string.Concat(obj.name, m_suffix);
            }
        }

        void NumberObjects()
        {
            int count = 0;
            foreach (GameObject obj in m_SelectedObjects)
            {
                count ++;
                obj.name = string.Concat(obj.name, count);
              
            }
        }

        void Replace()
        {
            foreach (GameObject obj in m_SelectedObjects)
            {
                if (obj.name.Contains(m_searchString))
                {
                    Debug.Log(obj.name);
                    obj.name = obj.name.Replace(m_searchString, m_replaceString);
                    Debug.Log(obj.name);
                }
            }

        }




        void DialogDisplay(string aMessage)
        {
            EditorUtility.DisplayDialog(m_dialogName + "Warning", aMessage, "OK");
        }
    }
}
        #endregion
    