﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Emortal.Core
{
    public static class EF_UI_Helpers 
    {
        public static void CreateUIGroup()
        {
            GameObject uiGrp = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Emortal_Framework/UI/Prefabs/UI_GRP.prefab", typeof(GameObject));
            if(uiGrp)
            {
                GameObject curUIGrp = GameObject.Instantiate(uiGrp);
                curUIGrp.name = "UI_GRP";
            }
            else
            {
                EF_Editor_Utils.DisplayDialogBox("Unable to Find the UI_GRP Prefab in any folder!");
            }
        }
    }
}
